﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;


namespace WebBudgetApp.Controllers
{
    public class ExpensesController : Controller
    {
        private Dictionary<string, float> data;
        private float totalAmount;
        private float totalExpenditure;

        /// <summary>
        /// Returns expenses page
        /// </summary>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Calculates the expenses section
        /// </summary>
        [HttpPost]
        public JsonResult CalculateExpenses()
        {
            data = new Dictionary<string, float>();

            for (int i = 0; i < Request.Form.Count; i++)
            {
                if (Request.Form[i] == "")
                {
                    continue;
                }
                else
                {
                    float value = Convert.ToSingle(Request.Form[i]);
                    string name = Request.Form.Keys[i];

                    data.Add(name, value);
                }
            }

            RunExpenseCalculation();

            return Json(new List<float> { totalAmount, totalExpenditure });
        }

        /// <summary>
        /// Runs the calculation on the expenses
        /// </summary>
        private void RunExpenseCalculation()
        {
            foreach (KeyValuePair<string, float> values in data)
            {
                switch (values.Key)
                {
                    case "Salary":
                    case "Bonus":
                    case "Overtime":
                    case "Benefits":
                    case "Opening Balance":
                        totalAmount += values.Value;
                        continue;
                    default:
                        totalAmount -= values.Value;
                        totalExpenditure += values.Value;
                        continue;
                }
            }
        }
    }
}
