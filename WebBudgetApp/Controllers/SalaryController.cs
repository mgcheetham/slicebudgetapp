﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web.Helpers;
using System.Web.Mvc;
using WebBudgetApp.Models.TaxCalculator;

[assembly: InternalsVisibleTo("WebBudgetAppTest")]

namespace WebBudgetApp.Controllers
{
    public class SalaryController : Controller
    {
        private TableData tableData;

        private decimal userSalaryYearly;
        private string userTaxCode;
        private decimal userPensionMonthly;
        private decimal userUniMonthlyRepayment;
        private int userWeeklyHours;

        /// <summary>
        /// Returns index page
        /// </summary>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Takes salary data and calculates outputs
        /// </summary>
        [HttpPost]
        [ValidateCustomAntiForgeryToken]
        public ActionResult GetSalaryTaxCalculation(Dictionary<string, string> form)
        {
            //ValidateCSRFToken();

            tableData = new TableData();

            ConvertUserInput(form);

            CalculateSalary();
            CalculateTaxableAmount();
            CalculateTax();
            CalculateNITax();
            CalculatePensionContribution();
            CalculateUniRepayments();
            CalculateYearlyNet();

            dynamic[] dataArray = { tableData.salary, tableData.taxableAmount,
                tableData.deductedTax, tableData.deductedPension,
                tableData.deductedUniversity, tableData.totalNet };

            return Json(dataArray);
        }

        /// <summary>
        /// Converts user input form data to properties
        /// </summary>
        private void ConvertUserInput(Dictionary<string, string> userInputData)
        {
            userSalaryYearly = Convert.ToDecimal(userInputData.Where(x => x.Key == "Salary").Select(v => v.Value).ElementAt(0));
            userTaxCode = Convert.ToString(userInputData.Where(x => x.Key == "TaxCode").Select(v => v.Value).ElementAt(0));
            userPensionMonthly = Convert.ToDecimal(userInputData.Where(x => x.Key == "Pension").Select(v => v.Value).ElementAt(0));
            userUniMonthlyRepayment = Convert.ToDecimal(userInputData.Where(x => x.Key == "UniversityLoan").Select(v => v.Value).ElementAt(0));
            userWeeklyHours = Convert.ToInt16(userInputData.Where(x => x.Key == "WeeklyHoursWorked").Select(v => v.Value).ElementAt(0));
        }

        /// <summary>
        /// Calculates the salary breakdown
        /// </summary>
        private void CalculateSalary()
        {
            tableData.salary = new Salary();

            RoundAndAssignAllData(tableData.salary, userSalaryYearly);
        }

        /// <summary>
        /// Calculates the taxable amount breakdown
        /// </summary>
        private void CalculateTaxableAmount()
        {
            tableData.taxableAmount = new TaxableAmount();

            int tax = 0;
            decimal taxable = 0;

            if (!string.IsNullOrEmpty(userTaxCode) & userTaxCode == "1257L")
            {
                tax = 12570;
                taxable = (userSalaryYearly - tax);
            }

            RoundAndAssignAllData(tableData.taxableAmount, taxable);
        }

        /// <summary>
        /// Actual tax calculation from taxable amount
        /// </summary>
        private void CalculateTax()
        {
            tableData.deductedTax = new DeductedTax();

            decimal decimalValue = (20.00M / 100);
            decimal totalYearlyTax = (decimalValue * tableData.taxableAmount.Yearly);

            RoundAndAssignAllData(tableData.deductedTax, totalYearlyTax);
        }

        /// <summary>
        /// Actual NI tax calculation
        /// </summary>
        private void CalculateNITax()
        {
            tableData.deductedNI = new DeductedNI();

            decimal decimalValue = 7.857M / 100;
            decimal niYearlyTotal = decimalValue * tableData.salary.Yearly;

            RoundAndAssignAllData(tableData.deductedNI, niYearlyTotal);
        }

        /// <summary>
        /// Calculates pension amount if required
        /// </summary>
        private void CalculatePensionContribution()
        {
            tableData.deductedPension = new DeductedPension();

            decimal pensionYearlyContribution = (userPensionMonthly * 12);

            RoundAndAssignAllData(tableData.deductedPension, pensionYearlyContribution);
        }

        /// <summary>
        /// Calculates university loan if required
        /// </summary>
        private void CalculateUniRepayments()
        {
            tableData.deductedUniversity = new DeductedUniversity();

            decimal uniYearlyRepayment = (userUniMonthlyRepayment * 12);

            RoundAndAssignAllData(tableData.deductedUniversity, uniYearlyRepayment);
        }

        /// <summary>
        /// Calculates yearly net amount
        /// </summary>
        private void CalculateYearlyNet()
        {
            tableData.totalNet = new TotalNet();

            decimal totalYearlyNet = (tableData.salary.Yearly -
                tableData.deductedTax.Yearly - tableData.deductedNI.Yearly - 
                tableData.deductedPension.Yearly - tableData.deductedUniversity.Yearly);

            RoundAndAssignAllData(tableData.totalNet, totalYearlyNet);
        }

        /// <summary>
        /// Rounds all data and assigns calculated values to given object
        /// </summary>
        private void RoundAndAssignAllData(dynamic deductions, decimal amount)
        {
            deductions.Yearly = Math.Round(amount, 2);
            deductions.Monthly = Math.Round((amount / 12), 2);
            deductions.Weekly = Math.Round((amount / 52), 2);
            deductions.Hourly = Math.Round((amount / 52 / userWeeklyHours), 2);
        }
    }
}
