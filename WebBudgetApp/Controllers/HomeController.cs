﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;


namespace WebBudgetApp.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Returns index page for application
        /// </summary>
        public ActionResult Index()
        {
            ViewBag.Title = "Slice";
            ViewBag.Version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

            return View();
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ValidateCustomAntiForgeryTokenAttribute : FilterAttribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            //throw new NotImplementedException();
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext != null && filterContext.HttpContext != null &&
                filterContext.HttpContext.Request != null &&
                !string.IsNullOrEmpty(filterContext.HttpContext.Request.HttpMethod)
                && string.Compare(filterContext.HttpContext.Request.HttpMethod, "POST", true) == 0)
            {
                ValidateRequestHeader(filterContext.HttpContext.Request);
            }
        }

        private void ValidateRequestHeader(HttpRequestBase request)
        {
            string cookieToken = "";
            string formToken = "";

            IEnumerable<string> tokenHeaders = request.Headers.GetValues("RequestVerificationToken");

            if (tokenHeaders != null && tokenHeaders.Count() > 0)
            {
                string[] tokens = tokenHeaders.First().Split(':');

                if (tokens.Length == 2)
                {
                    cookieToken = tokens[0].Trim();
                    formToken = tokens[1].Trim();
                }

                try
                {
                    AntiForgery.Validate(cookieToken, formToken);
                }
                catch (HttpAntiForgeryException ex)
                {
                    var msg = ex.Message;
                    throw new HttpAntiForgeryException(msg);
                    // Still want to return a successful json request
                    // just add an error message or false value
                }
            }
        }
    }
}