﻿class UserDetails {

    constructor(name, age) {
        this.name = name;
        this.age = age;
    }

    userDetails() {

        console.log(this.name, this.age);
    }
}

class JobDetails {

    constructor(type, location) {
        this.type = type;
        this.location = location;
    }

    createNewJob() {

        return "Banker";
    }

    updateSuperUserDetails(location) {

        this.location = location;
    }
}

class SuperUser extends UserDetails {

    // Protected properties
    _specialKey = 0;

    constructor(name, age, role, access, type, location) {
        super(name, age);
        this.role = role;
        this.access = access;

        this.jobDetails = new JobDetails(type, location);
    }

    //// Setter properties
    //set key(number) {
    //    this._specialKey = number;
    //}

    //// Readonly property
    //get role() {
    //    return this.role
    //}

    getDetails() {

        console.log(
            this.name,
            this.age,
            this.role,
            this.access,
            this.jobDetails.location,
            this.jobDetails.type
        );
    }

    getUserDetails() {

        // Call UserDetails function
        super.userDetails();
    }

    updateJobType() {

        this.role = this.jobDetails.createNewJob();
    }

    updateSuperUserDetails(location) {

        this.jobDetails.updateSuperUserDetails(location);
    }

    static testStaticMethod() {

    }
}