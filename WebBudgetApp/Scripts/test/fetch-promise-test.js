﻿// Async version not with promises
async function getUrlAsync() {

    let response = await fetch("https://jsonplaceholder.typicode.com/todos/1");

    if (response.ok) {

        let json = await response.json();

        console.log("User ID: " + json.userId + " from " + response.url);
    } else {
        console.log("Unable to load. HTTP-Error: " + response.status);
    }
}

async function postUrlAsync() {

    let data = {
        "Salary": "30000",
        "TaxCode": "1250",
        "WeeklyHoursWorked": "39",
        "Pension": "75",
        "UniversityLoan": "0"
    };

    let response = await fetch("Salary/PostNewTest", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({form: data})
    });

    if (response.ok) {

        let json = await response.json();

        console.log(json);
    } else {
        console.log(json);
    }
}

// Promise version
async function getUrlPromise() {

    await fetch('https://jsonplaceholder.typicode.com/todos/1')
        .then(response => {
            if (!response.ok) throw Error(response.statusText); return response;
        })
        .then(response => response.json())
        .then(id => console.log(id))
        .catch(error => console.log(error));
}

async function getUrlsPromiseAll() {

    let urls = [
        'https://api.github.com/users/iliakan',
        'https://api.github.com/users/remy',
        'https://api.github.com/users/jeresig'
    ];

    // Map every url to the promise of the fetch
    let requests = urls.map(url => fetch(url));

    // Promise.all waits until all jobs are resolved
    await Promise.all(requests)
        .then(responses => responses.forEach(
            response => console.log(`${response.url}: ${response.status}`)
        ))
        .catch(error => console.log(error));
}

async function fetchAllUrlsAsync() {

    let urls = [
        'https://jsonplaceholder.typicode.com/todo/1',
        'https://jsonplaceholder.typicode.com/todos/2',
        'https://jsonplaceholder.typicode.com/todos/3'
    ];

    let requests = [];

    for (let url of urls) {

        await fetch(url)
            .then(
                success => {

                    //console.log(success);
                    if (success.ok != true) {
                        //throw Error(success.statusText);
                        requests.push({});
                    }
                    else {
                        requests.push(success.json());
                    }
                },
                failure => {
                    console.log(failure);
                })
            .catch(error => console.log(error));

    }

    let results = await Promise.allSettled(requests)
        .then(responses => responses.forEach(
            response => console.log(response)
        ));
}

async function shortFetchAsync() {

    let urls = [
        'https://api.github.com/users/User1',
        'https://api.github.com/users/User2',
        'https://api.github.com/users/User9999999999'
    ];

    await Promise.allSettled(urls.map(url => fetch(url)))
        .then(results => {

            results.forEach((result, num) => {

                console.log(result);
                if (result.value.ok == false | result.value.status == 404) {
                    result.status = "rejected";
                }
            });

            return results;
        })
        .then(responses => Promise.allSettled(responses.map(result => result.json())))
        .then(users => users.forEach(user => console.log(user)));

}


//getUrlAsync();
//getUrlPromise();
//getUrlsPromiseAll();
//fetchAllUrlsAsync();
//shortFetchAsync();