﻿async function fetchRequest(items) {

    var requests = items.map(item => fetch(item));

    return Promise.allSettled(requests);
}

function checkIfFulfilled(allResults) {

    const fulfilledResult = [];

    allResults.forEach((result) => {

        if (result.value.status === 200) {

            fulfilledResult.push(result.value);
        }
    });

    return fulfilledResult;
}

function request() {

    let urls = [
        'Salary/GetSalaryCalculation',
        'https://jsonplaceholder.typicode.com/todos/2',
        'https://jsonplaceholder.typicode.com/todos/3'
    ];

    fetchRequest(urls, dataToSend)
        .then((allResults) => {

            return checkIfFulfilled(allResults);
        })
        .then(result => Promise.allSettled(result.map(x => x.json())))
        .then(result => console.log(result));
}