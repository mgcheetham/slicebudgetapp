﻿$(function () {
    ChartJS.initialise();
})

var pieChart;
var areaChart;

var ChartJS = {

    vars: {

    },

    fn: {
        createPieChart: function (pie, data) {

            if (pieChart) {

                pieChart.destroy();
            }

            pieChart = new Chart(pie, {
                type: 'pie',
                data: {
                    labels: [

                    ],
                    datasets: [{
                        data: [

                        ],
                        borderColor: [
                            '#7cf44c',
                            '#3cb05b',
                            '#54d899',
                            '#5492d8',
                            '#337cdf',
                            '#a15dea',
                            '#b133c9',
                            '#e55d5a',
                            '#ffe26b',
                            '#9af9f2',
                            '#ffa8e7',
                            '#70f1be',
                            '#fea7a7',
                            '#3be0d9',
                            '#3a2f83',
                            '#ff72b1',
                        ],
                        borderWidth: 1,
                        backgroundColor: [
                            '#7cf44c',
                            '#3cb05b',
                            '#54d899',
                            '#5492d8',
                            '#337cdf',
                            '#a15dea',
                            '#b133c9',
                            '#e55d5a',
                            '#ffe26b',
                            '#9af9f2',
                            '#ffa8e7',
                            '#70f1be',
                            '#fea7a7',
                            '#3be0d9',
                            '#3a2f83',
                            '#ff72b1',
                        ]
                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: true,
                    responsiveAnimationDuration: 0,
                    animation: {
                        animateRotate: true,
                    },
                    legend: {
                        display: false,
                    }
                }
            });

            data.forEach(function (index, value) {

                pieChart.data.datasets[0].data.push(index);
                pieChart.data.labels.push(value);
            });

            pieChart.reset();
            pieChart.update();
        },

        createAreaChart: function (area, data) {

            areaChart = new Chart(area, {
                type: 'line',
                data: {
                    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
                    datasets: [{
                        data: [1652, 1436, 1402, 1719, 1699, 1601, 1698, 1700, 1450, 1587, 1608, 1606],
                        label: "Total Expense £",
                        borderColor: "#8e5ea2",
                        fill: true
                    }, {
                            data: [1756, 1756, 1756, 1756, 1756, 1756, 1756, 1756, 1756, 1756, 1756, 1756],
                            label: "Income £",
                            borderColor: "#3e95cd",
                            fill: true
                    }
                    ]
                },
                options: {
                    title: {
                        display: false,
                        text: 'Yearly Financial'
                    },
                    plugins: {
                        filler: {
                            propagate: true
                        }
                    }
                }
            });
        }
    },

    initialise: function () {

    }
}

