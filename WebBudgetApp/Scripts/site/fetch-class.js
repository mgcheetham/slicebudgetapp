﻿class Fetch {

    constructor(url, data) {
        this.url = url;
        this.data = data;
    }

    getRequest() {
 
    }

    async postRequestAsync() {

        var csrfToken = $("#token").val();

        let response = await fetch(this.url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'RequestVerificationToken': csrfToken
            },
            body: JSON.stringify({ form: this.data }),
        })
            .catch(error => console.log(error))

        return await response;
    }

    GetMultipleRequests() {

    }

    postMultipleRequests() {


    }
}