﻿class SalaryCalculator {

    _fetchRequest;

    constructor(url) {
        this.url = "/Salary/GetSalaryTaxCalculation";
    }

    getData() {

        this.data = {
            "Salary": $("#salaryAmount").val(),
            "TaxCode": $("#taxCode").val(),
            "WeeklyHoursWorked": $("#hoursWorked").val(),
            "Pension": $("#pensionValidation").val(),
            "UniversityLoan": $("#studentValidation").val(),
        };
    }

    getSalaryData() {

        this._fetchRequest = new Fetch(this.url, this.data);

        this._fetchRequest.postRequestAsync()
            .then(response => {
                if (!response.ok) throw Error(response.statusText); return response;
            })
            .then(response => { return response.json() })
            .then(jsonResponse => this.convertOutputData(jsonResponse))
            .catch (error => console.log(error));
    }

    convertOutputData(data) {

        // Get the table body to add data
        var table = document.getElementById("taxTableBody");

        // Clear table first
        table.innerHTML = "";

        // Build new table data
        for (let i = 0; i < data.length; i++) {

            var newRow = table.insertRow(i);

            var newCell = newRow.insertCell(0);
            newCell.innerHTML = data[i].Name;

            var newCell = newRow.insertCell(1);
            newCell.innerHTML = `£ ${data[i].Yearly}`;

            var newCell = newRow.insertCell(2);
            newCell.innerHTML = `£ ${data[i].Monthly}`;

            var newCell = newRow.insertCell(3);
            newCell.innerHTML = `£ ${data[i].Hourly}`;

            var newCell = newRow.insertCell(4);
            newCell.innerHTML = `£ ${data[i].Weekly}`;
        }
    }
}