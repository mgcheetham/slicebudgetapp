﻿var Ajax = {

    vars: {

    },

    fn: {
        ajaxPost: function (sFunction, onSuccess, onError, data, passThroughData, iTimeoutMillis) {

            if (typeof iTimeoutMillis !== "number") {
                iTimeoutMillis = 30000;
            }
            if (typeof passThroughData === "undefined") {
                passThroughData = {};
            }
            if (typeof onSuccess !== "function") {
                onSuccess = function () { };
            }
            if (typeof onError !== "function") {
                onError = function () { };
            }
            if (typeof data === "undefined") {
                data = null;
            }

            var sCsrfToken = $("input[name='__RequestVerificationToken']").val();
            if (typeof sCsrfToken !== "string") {
                sCsrfToken = "";
            }

            // Make ajax call
            $.ajax({
                type: "POST",
                url: sFunction,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                headers: { "X-XSRF-Token": sCsrfToken },
                cache: false,
                data: data === null ? null : JSON.stringify(data),
                success: function (response, status, jqXhr) {

                    if (typeof response.d !== "undefined") {
                        onSuccess(response.d, status, jqXhr, passThroughData);

                    } else {
                        onSuccess(response, status, jqXhr, passThroughData);
                    }
                },
                error: function (jqXhr, status, errorName) {
                    // Handle generic errors if they exist, otherwise forward to error handler

                    if (jqXhr.status === 401) {
                        // Unauthorised. Force a refresh
                        window.location.href = window.location.href;
                        return;
                    }
                    else if (status === "timeout") {
                        // Function call timeout

                    }
                    onError(jqXhr, status, errorName, passThroughData);
                },
                timeout: iTimeoutMillis,
            });
        },
    },
}