﻿var AjaxForm = {

    vars: {

    },

    fn: {
        ajaxPostForm: function (postUrl, onSuccess, onError, data, passThroughData, iTimeoutMillis) {

            if (typeof iTimeoutMillis !== "number") {
                iTimeoutMillis = 30000;
            }
            if (typeof passThroughData === "undefined") {
                passThroughData = {};
            }
            if (typeof onSuccess !== "function") {
                onSuccess = function () { };
            }
            if (typeof onError !== "function") {
                onError = function () { };
            }
            if (typeof data === "undefined") {
                data = null;
            }

            var csrfToken = $("input[name='__RequestVerificationToken']").val();

            if (typeof csrfToken !== "string") {
                csrfToken = "";
            }

            // Make ajax call
            $.ajax({
                type: "POST",
                headers: { '__RequestVerificationToken': csrfToken },
                url: postUrl,
                contentType: false,
                //dataType: false,
                cache: false,
                processData: false,
                data: data === null ? null : data,
                success: function (response, status, jqXhr) {

                    if (typeof response.d !== "undefined") {

                        onSuccess(response.d, status, jqXhr, passThroughData);

                    } else {

                        onSuccess(response, status, jqXhr, passThroughData);
                    }
                },
                error: function (jqXhr, status, errorName) {

                    // Handle generic errors if they exist
                    if (jqXhr.status === 401) {

                        // Unauthorised. Force a refresh
                        window.location.href = window.location.href;
                        return;
                    }
                    else if (status === "timeout") {

                        // Function call timeout

                    }

                    onError(jqXhr, status, errorName, passThroughData);
                },
                timeout: iTimeoutMillis,
            });
        },
    },
}