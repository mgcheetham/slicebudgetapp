﻿$(function () {
    Budget.initialise();
});

var Budget = {

    fn: {
        callAddModal: function () {

            var count = 0;

            // call description builder
            Budget.fn.buildDescriptions();

            $("#getSave").on("click", function () {

                var presetDesc = $("#getPresetDescription").val();
                var textDesc = $("#getTextDescription").val();
                console.log(presetDesc, textDesc);

                if (textDesc == "") {

                    Budget.fn.buildNewField(presetDesc);
                }

                if (presetDesc == "") {

                    Budget.fn.buildNewField(textDesc);
                }

                // limits add count to 10
                count += 1;

                if (count == 10) {

                    $("#getSave").prop("disabled", true);
                    $("#limitReached").text("Maximum amount of fields allowed");
                }
            });

            $("#removeField").on("click", function () {

                // build removal of created input fields
            });
        },

        buildDescriptions: function () {

            var descriptions = {

                "Sky TV": "getSkyTv",
                "Virgin Media TV": "getVirginMediaTv",
                "Talk Talk TV": "getTalkTalkTv",
                "Xbox Live": "getXboxLive",
                "PSN": "getPsn",
                "iTunes": "getItunes",
                "Book Club": "getBookClub",
                "Dentist": "getDentist",
                "Hair Cut": "getHairCut"
            };

            $.each(descriptions, function (index, value) {

                $("#getPresetDescription").append(`"<option id="${value}">${index}</option>"`);
            });
        },

        buildNewField: function (description) {

            // check section values
            var checkHome = $("#checkHome").html();
            var checkPersonal = $("#checkPersonal").html();
            var checkOther = $("#checkOther").html();

            // get new section values
            var selected = $("#selectSection option:selected").html();
            var amount = $("#getAmount").val();

            if (selected == checkHome || checkPersonal || checkOther != "" || null) {

                $("#" + selected).append(
                    `<div class='input-group input-group-sm mb-3'>
                    <div class='input-group-prepend'>
                    <span class='input-group-text' style='width: 210px'>${description}</span>
                    </div>
                    <input id='' type='text' name='${description}' class='form-control' value='${amount}'>
                    </div>`
                );
            }
        },

        updateMonthlyBreakdown: function () {

            var monthlyData = new FormData($("#monthlyBreakdownData")[0]);

            //console.log(Array.from(formData));

            var pie = $("#pieChart");
            var area = $("#areaChart");

            ChartJS.fn.createPieChart(pie, monthlyData);
            ChartJS.fn.createAreaChart(area, monthlyData);
        },

        calculateMonthlyBalance: function () {

            var monthlyData = new FormData($("#monthlyBreakdownData")[0]);
            monthlyData.append("Opening Balance", $("#openingBalance").val());

            AjaxForm.fn.ajaxPostForm("/Expenses/CalculateExpenses",
                function (response) {

                    $.each(response, function (index, value) {

                        if (index == 0) {

                            $("#closingBalance").val(value);
                        }
                        else {

                            $("#totalExpenditure").val(value)
                        }
                    });
                    
                },
                function (xhr, status, error) {

                    console.log(xhr);
                    console.log(status);
                    console.log(error);
                },
                monthlyData,
            );
        }
    },

    initialise: function () {

        // Begins process when add item modal is invoked
        $("#getAdd").on("click", function () {

            Budget.fn.callAddModal();
        });

        var timeout;
        var tabKeyPressed = false;

        $("#monthlyBreakdownData").keydown(function (e) {

            tabKeyPressed = e.keyCode == 9;

            if (tabKeyPressed) {

                //e.preventDefault();
                return;
            }
        });

        // Begins process to update monthly breakdown
        $("#monthlyBreakdownData").on("keyup", function () {

            if (tabKeyPressed == false) {

                clearTimeout(timeout);

                timeout = setTimeout(function () {

                    Budget.fn.updateMonthlyBreakdown();
                }, 900);
            }
        });

        // Calculates monthly amounts
        $("#updateBalance").on("click", function () {

            Budget.fn.calculateMonthlyBalance();
        });
    }
}