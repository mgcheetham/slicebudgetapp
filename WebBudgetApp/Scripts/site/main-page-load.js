﻿$(document).ready(function () {

    salaryCalculator();
    monthlyBudget();
});

function salaryCalculator() {

    // Salary calc checkbox on change check 
    $("input[type=checkbox]").on("change", function () {

        $(".form-control").prop('required', true);
    });

    // Salary calc on button finish gets table data
    $("#calculateAmount").on("click", function () {

        // Initialise new salary calculator
        let salaryCalc = new SalaryCalculator();
        salaryCalc.getData();
        salaryCalc.getSalaryData();
    });
}

function monthlyBudget() {

    // Begins process when add item modal is invoked
    $("#getAdd").on("click", function () {

        Budget.fn.callAddModal();
    });

    var timeout;
    var tabKeyPressed = false;

    $("#monthlyBreakdownData").keydown(function (e) {

        tabKeyPressed = e.keyCode == 9;

        if (tabKeyPressed) {

            //e.preventDefault();
            return;
        }
    });

    // Begins process to update monthly breakdown
    $("#monthlyBreakdownData").on("keyup", function () {

        if (tabKeyPressed == false) {

            clearTimeout(timeout);

            timeout = setTimeout(function () {

                Budget.fn.updateMonthlyBreakdown();
            }, 900);
        }
    });

    // Calculates monthly amounts
    $("#updateBalance").on("click", function () {

        Budget.fn.calculateMonthlyBalance();
    });
}