﻿using System.Web;
using System.Web.Mvc;
using WebBudgetApp.Controllers;

namespace WebBudgetApp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new ValidateCustomAntiForgeryTokenAttribute());
        }
    }
}
