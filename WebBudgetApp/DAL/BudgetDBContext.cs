﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using WebBudgetApp.Models.MonthlyExpenses;
using WebBudgetApp.Models.TaxCalculator;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace WebBudgetApp.DAL
{
    public class BudgetDBContext : DbContext
    {
        /// <summary>
        /// Ensure same as connection string in web.config
        /// </summary>
        public BudgetDBContext() : base("BudgetDBContext")
        {

        }

        public DbSet<MonthlyExpense> Expenses { get; set; }
        public DbSet<SalaryInput> SalaryCalculator { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Ensures table names don't get pluralised
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}