﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using WebBudgetApp.Models;
using System.Data.Entity.Validation;

namespace WebBudgetApp.DAL
{
    public class Initialiser : DropCreateDatabaseIfModelChanges<BudgetDBContext>
    {
        protected override void Seed(BudgetDBContext context)
        {

        }
    }
}