﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebBudgetApp.Models.MonthlyExpenses
{
    public class MonthlyExpense
    {
        [Key]
        public int MonthlyId { get; set; }

        [Range(0.00, 9999999999.99)]
        [Display(Name = "Opening Balance")]
        [DataType(DataType.Currency)]
        public decimal OpeningBalance { get; set; }

        [Range(0.00, 9999999999.99)]
        [Display(Name = "Closing Balance")]
        [DataType(DataType.Currency)]
        public decimal ClosingBalance { get; set; }

        [StringLength(200)]
        [Display(Name = "Item Description")]
        public string ItemDescription { get; set; }

        [Range(0.00, 9999999999.99)]
        [Display(Name = "Item Amount")]
        [DataType(DataType.Currency)]
        public decimal ItemAmount { get; set; }

        //public virtual ICollection<SalaryCalculator> SalaryCalculator { get; set; }
    }
}