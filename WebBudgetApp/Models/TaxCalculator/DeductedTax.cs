﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebBudgetApp.Models.TaxCalculator
{
    public class DeductedTax: ICalculator
    {
        public string Name { get; set; } = "Income Tax";
        public decimal Yearly { get; set; } = 0;
        public decimal Monthly { get; set; } = 0;
        public decimal Weekly { get; set; } = 0;
        public decimal Hourly { get; set; } = 0;
    }
}