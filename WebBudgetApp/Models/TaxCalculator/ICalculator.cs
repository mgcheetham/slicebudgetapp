﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebBudgetApp.Models.TaxCalculator
{
    interface ICalculator
    {
        decimal Yearly { get; set; }
        decimal Monthly { get; set; }
        decimal Weekly { get; set; }
        decimal Hourly { get; set; }
    }
}
