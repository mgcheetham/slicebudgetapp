﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebBudgetApp.Models.TaxCalculator
{
    public class SalaryInput
    {
        [Key]
        public int UserId { get; set; }

        [Required]
        [Display(Name = "Monthly Gross Salary")]
        public decimal Salary { get; set; }

        [Required]
        [Display(Name = "Weekly Hours")]
        public int WeeklyHours { get; set; }

        [Display(Name = "Overtime Hours")]
        public int OverTimeHours { get; set; }

        [Display(Name = "Tax Code")]
        [StringLength(30)]
        public string TaxCode { get; set; }

        [Display(Name = "Tax Deductable Amount")]
        public decimal Tax { get; set; }

        [Display(Name = "Monthly Net Amount")]
        public decimal MonthlyNet { get; set; }

        [Display(Name = "Weekly Net Amount")]
        public decimal WeeklyNet { get; set; }

        [Display(Name = "Hourly Rate Amount")]
        public decimal HourlyRate { get; set; }

        [Display(Name = "Monthly Overtime Net Amount")]
        public decimal OvertimeTotal { get; set; }

        [Display(Name = "Monthly Overtime Net Amount")]
        public decimal OvertimeSalaryTotal { get; set; }

        [Display(Name = "Monthly Pension Amount")]
        public decimal Pension { get; set; }

        [Display(Name = "Monthly Student Loan Amount")]
        public decimal StudentLoan { get; set; }
    }
}