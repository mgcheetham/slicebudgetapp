﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebBudgetApp.Models.TaxCalculator
{
    public class TableData
    {
        internal Salary salary;
        internal TaxableAmount taxableAmount;
        internal DeductedTax deductedTax;
        internal DeductedNI deductedNI;
        internal DeductedPension deductedPension;
        internal DeductedUniversity deductedUniversity;
        internal TotalNet totalNet;
    }
}