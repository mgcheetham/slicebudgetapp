﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace WebBudgetApp.Models.TaxCalculator
{
    public enum TaxCodes
    {
        [Description("Under 65")]
        L,
        [Description("Married")]
        M,
        [Description("Higher allowance")]
        Y,
        [Description("Basic rate")]
        R,
        [Description("Higher rate")]
        D,
        [Description("Additional rate")]
        DD,
        [Description("No tax")]
        NT
    }
}