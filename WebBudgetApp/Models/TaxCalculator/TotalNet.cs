﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebBudgetApp.Models.TaxCalculator
{
    public class TotalNet: ICalculator
    {
        public string Name { get; set; } = "Income Net";
        public decimal Yearly { get; set; } = 0;
        public decimal Monthly { get; set; } = 0;
        public decimal Weekly { get; set; } = 0;
        public decimal Hourly { get; set; } = 0;
    }
}