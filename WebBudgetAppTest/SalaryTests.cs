﻿using System;
using System.Web.Mvc;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebBudgetApp.Controllers;
using WebBudgetApp.Models;
using System.Web.Routing;
using System.Diagnostics;

namespace WebBudgetAppTest
{
    [TestClass]
    public class SalaryTests
    {
        private SalaryController salary;

        public SalaryTests()
        {
            salary = new SalaryController();
        }

        [TestMethod]
        public void TestForm()
        {
            Dictionary<string, string> form = new Dictionary<string, string>();
            form.Add("Salary", "30000");
            form.Add("WeeklyHoursWorked", "39");
            form.Add("TaxCode", "1250");
            form.Add("Pension", "70");
            form.Add("UniversityLoan", "0");

            var result = salary.GetSalaryTaxCalculation(form);

            IDictionary<string, object> data = new RouteValueDictionary(result);

            Assert.AreEqual(30000M, data["Salary"], "Incorrect data");
        }
    }
}
